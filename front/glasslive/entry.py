from typing import Optional, Callable
import asyncio

from core.backend import Backend
from util.misc import Logger
import settings

from .glass import GlassCtrl

def register(loop: asyncio.AbstractEventLoop, backend: Backend) -> None:
    from util.misc import ProtocolRunner
    from . import glasslive

    backend.add_runner(ProtocolRunner('0.0.0.0', 27002, ListenerGlass, args = ['BG', backend, glasslive.GlassCtrlLive]))

class ListenerGlass(asyncio.Protocol):
    logger: Logger
    backend: Backend
    controller: GlassCtrl
    transport: Optional[asyncio.WriteTransport]

    def __init__(self, logger_prefix: str, backend: Backend, controller_factory: Callable[[Logger, str, Backend], GlassCtrl]) -> None:
        super().__init__()
        self.logger = Logger(logger_prefix, self, settings.DEBUG_GLASS)
        self.backend = backend
        self.controller = controller_factory(self.logger, 'direct', backend)
        self.controller.close_callback = self._on_close
        self.transport = None

    def connection_made(self, transport: asyncio.BaseTransport) -> None:
        assert isinstance(transport, asyncio.WriteTransport)
        self.transport = transport
        self.logger.log_connect()
        self.controller.on_connect()
    
    def connection_lost(self, exc: Optional[Exception]) -> None:
        self.controller.close()
        self.logger.log_disconnect()
        self.transport = None

    def data_received(self, data: bytes) -> None:
        transport = self.transport
        assert transport is not None
        # Setting `transport` to None so all data is held until the flush
        #self.controller.transport = None
        if self.controller.transport is None:
            self.controller.transport = self.transport
        self.controller.data_received(data)
        #transport.write(self.controller.flush())
        #self.controller.transport = transport

    def _on_close(self) -> None:
        if self.transport is None: return
        self.transport.close()