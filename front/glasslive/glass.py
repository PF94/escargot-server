from abc import abstractmethod
from typing import Tuple, Optional, Iterable, List, Any, Callable, Dict
import io
import asyncio
import json
import re

from util.misc import Logger

class GlassCtrl:

    logger: Logger
    reader: 'GlassReader'
    writer: 'GlassWriter'
    close_callback: Optional[Callable[[], None]]
    transport: Optional[asyncio.WriteTransport]

    def __init__(self, logger: Logger) -> None:
        self.logger = logger
        self.reader = GlassReader(logger)
        self.writer = GlassWriter(logger)
        self.close_callback = None
        self.closed = False
        self.transport = None

    @abstractmethod
    def on_connect(self) -> None: pass
            
    def data_received(self, data: bytes, *, transport: Optional[asyncio.BaseTransport] = None) -> None:
        if transport is None:
            transport = self.transport
        assert transport is not None
        self.peername = transport.get_extra_info('peername')
        for line in data.splitlines():
            command = self.reader.data_received(line)
            for m in command:
                try:
                    f = getattr(self, '_m_{}'.format(m["type"]))
                    list = [[item, m[item]] for item in m]
                    f(*list[1:])
                except Exception as ex:
                    self.logger.error(ex) 

    def close(self) -> None:
        if self.closed: return
        self.closed = True
        if self.close_callback:
            self.close_callback()
        self._on_close()

    def send_reply(self, *m: Any) -> None:
        self.writer.write(m)
        transport = self.transport
        if transport is not None:
            transport.write(self.flush())

    def flush(self) -> bytes:
        return self.writer.flush()

    @abstractmethod
    def _on_close(self) -> None: pass

class GlassWriter:
    __slots__ = ('_logger', '_buf')
    
    _logger: Logger
    _buf: io.BytesIO
    
    def __init__(self, logger: Logger) -> None:
        self._logger = logger
        self._buf = io.BytesIO()

    def write(self, m: Iterable[Any]) -> None:
        m_json = json.dumps(m) # might fuck up if not dictionary
        m_json_clean = m_json.strip("[]") # glass won't parse without this
        m_encoded = m_json_clean.encode() # encode
        self._buf.write(m_encoded) # send encode
        self._buf.write(b'\r\n') # oh yeah send new line as well
        self._logger.info('<<<', m_encoded.decode()) # remove b letter in logger
    
    def flush(self) -> bytes:
        data = self._buf.getvalue()
        if data:
            self._buf = io.BytesIO()
        return data

class GlassReader:
    __slots__ = ('logger', '_data')

    logger: Logger
    _data: bytes

    def __init__(self, logger: Logger) -> None:
        self.logger = logger
        self._data = b''

    def data_received(self, data: bytes) -> Iterable[List[Any]]:
        self._data = data
        while self._data:
            m = self._read_glass()
            if m is None: break
            return m
    
    def _read_glass(self) -> Optional[List[Any]]:
        try:
            m = _glass_try_decode(self._data)
        except Exception as ex:
            m = None
            self.logger.error(ex)

        self.logger.info('>>>', m)
        yield m

def _glass_try_decode(d: bytes) -> Tuple[List[Any], Optional[bytes], int]:
        m = json.loads(d)
        type = m
        return type